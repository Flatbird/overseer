const Eris = require('eris');
const config = require('./config.js');

let RESTClient = '';

RESTClient = new Eris(`Bot ${config.token}`, {restMode: true});

module.exports = RESTClient;