const fs = require('fs');
const client = require('./client.js');
require('./Functions/functions.js')(client);
require('./Logs/setup.js');
function read() {
  fs.readdir(__dirname + '/Commands', (err, files) => {
    if (err) console.error(err);
    const jsfiles = files.filter(f => f.split('.').pop()==='js');
    jsfiles.forEach(f=>{
      const props = require(__dirname + `/Commands/${f}`);
      client.registerCommand(props.name, props.action, props.options);
    });
  });
}
read();

fs.readdir(__dirname + '/Events', (err, files) => {
  if (err) return client.logger.error(err);
  files.forEach(file => {
    const event = require(__dirname + `/Events/${file}`);
    const eventName = file.split('.')[0];
    client.on(eventName, event.bind(null, client));
  });
});

client.connect();
client.logger.shard(`Starting bot with ${client.config.maxShards} shards...`);

process
  .on('disconnect', () => {
    client.logger.ded('Process Disconnected.');
  })
  .on('uncaughtException', (e) => {
    client.logger.ded(e.stack);
    client.createMessage('510604742351781903', `${client.config.em.e} **${e.message}**`);
  })
  .on('unhandledRejection', (e) => {
    client.logger.ded(e.stack);
    process.exit(1);
  });