const Eris = require('eris');
const config = require('./config.js');

let Aclient = '';
  
Aclient = new Eris.CommandClient(config.token, {
  defaultImageFormat: 'png',
  defaultImageSize: '128',
}, {
  guildOnly: true,
  prefix: 'o*',
  defaultHelpCommand: false,
  defaultCommandOptions: {
    requirements: {
      userIDs: [config.owner]
    },
    guildOnly: true,
    caseInsensitive: true,
  }
});

Aclient.config = config;

const fs = require('fs');
require('./Functions/functions.js')(Aclient);

function read(fld) {
  fs.readdir(__dirname + `/${fld}/`, (err, files) => {
    if (err) console.error(err);
    const jsfiles = files.filter(f => f.split('.').pop()==='js');
    jsfiles.forEach(f=>{
      const props = require(__dirname + `/${fld}/${f}`);
      Aclient.registerCommand(props.name, props.action, props.options);
    });
  });
}

read('/Commands');

Aclient.connect();

module.exports = Aclient;