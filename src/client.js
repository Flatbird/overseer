const Eris = require('eris');
const {Signale} = require('signale');
const config = require('./config.js');
const Enmap = require('enmap');
Object.defineProperty(Eris.Message.prototype, 'guild', {
  get: function() { return this.channel.guild; }
});

let client = '';
  
client = new Eris.CommandClient(config.token, {
  defaultImageFormat: 'png',
  defaultImageSize: '128',
  maxShards: config.maxShards
}, {
  guildOnly: true,
  prefix: config.prefix,
  defaultHelpCommand: false,
  defaultCommandOptions: {
    guildOnly: true,
    caseInsensitive: true,
  }
});

client.config = config;
const options = {
  disabled: false,
  interactive: false,
  stream: process.stdout,
  scope: 'bot',
  types: {
    shard: {
      badge: '🥝',
      color: 'blueBright',
      label: 'shard'
    },
    ded: {
      badge: '🚨',
      color: 'redBright',
      label: 'error'
    },
    ready: {
      badge: '👌',
      color: 'green',
      label: 'ready'
    }
  }
};
client.logger = new Signale(options);
client.settings = new Enmap({
  name: 'settings', 
  autoFetch: true
});

const logo = require('asciiart-logo');
console.log(
  logo({
    name: 'Overseer',
    font: 'Big',
    lineChars: 15,
    padding: 5,
    margin: 2
  })
    .emptyLine()
    .right('by Flatbird and fredoomed')
    .emptyLine()
    .render()
);

module.exports = client;