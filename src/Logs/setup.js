const client = require('../client.js');
const config = require('../config.js');

const s = client.registerCommand('setup', (msg) => {
  msg.channel.createMessage('<a:hi:510612865493762051> **Looking to setup logs?**\nI can help you!\n**First:**\nEnsure I have the following permissions in **all** the channels you\'d like me to send log messages in!\n+ <:green_tick:508736384123666432> `Read Messages`\n+ <:green_tick:508736384123666432> `Send Messages`\n+ <:green_tick:508736384123666432> `Embed Links`\n+ <:green_tick:508736384123666432> `Attach Files`\n+ <:green_tick:508736384123666432> `Use External Emojis`\n**Next:**\nI have multiple logging modules! You\'ll need to setup each individually! \nSay `o.setup message #channel` to setup message logging (`Message Deletes, and Edits`)\nSay `o.setup user #channel` to setup user logging (`Role Updates, Nickname Changes, Avatar Changes`)\nSay `o.setup server #channel` to setup server logging (`User Join / Leave, Role Updates, Channel Delete, Channel Create`)\n`*` **All of the above commands require you to have the `Manage Server` permission!**');
}, {
  'description': 'Setup logs on your server',
  'usage': 'setup'
});

s.registerSubcommand('message', (msg, args) => {
  if (args[0] == 'disable' || args[0] == 'off' || args[0] == 'null') {
    client.settings.setProp(msg.guild.id, 'messageLog', undefined);
    return msg.channel.createMessage(`${config.em.o} Disabled message log.`);
  }
  if (!msg.channelMentions[0]) return `${config.em.e} Please mention a channel.`;
  if (!client.settings.has(msg.guild.id)) client.settings.set(msg.guild.id, {});
  client.settings.setProp(msg.guild.id, 'messageLog', msg.channelMentions[0]);
  msg.channel.createMessage(`${config.em.o} Set message log channel to <#${msg.channelMentions[0]}>`);
}, {
  'description': 'Setup message logs on your server',
  'usage': 'setup message [off / #channel]',
  'requirements': {
    'permissions': {
      'manageGuild': true
    }
  }
});

s.registerSubcommand('user', (msg, args) => {
  if (args[0] == 'disable' || args[0] == 'off' || args[0] == 'null') {
    client.settings.setProp(msg.guild.id, 'messageLog', undefined);
    return msg.channel.createMessage(`${config.em.o} Disabled user log.`);
  }
  if (!msg.channelMentions[0]) return `${config.em.e} Please mention a channel.`;
  if (!client.settings.has(msg.guild.id)) client.settings.set(msg.guild.id, {});
  client.settings.setProp(msg.guild.id, 'userLog', msg.channelMentions[0]);
  msg.channel.createMessage(`${config.em.o} Set user log channel to <#${msg.channelMentions[0]}>`);
}, {
  'description': 'Setup logs on your server',
  'usage': 'setup user [off / #channel]',
  'requirements': {
    'permissions': {
      'manageGuild': true
    }
  }
});

s.registerSubcommand('server', (msg, args) => {
  if (args[0] == 'disable' || args[0] == 'off' || args[0] == 'null') {
    client.settings.setProp(msg.guild.id, 'messageLog', undefined);
    return msg.channel.createMessage(`${config.em.o} Disabled server log.`);
  }
  if (!msg.channelMentions[0]) return `${config.em.e} Please mention a channel.`;
  if (!client.settings.has(msg.guild.id)) client.settings.set(msg.guild.id, {});
  client.settings.setProp(msg.guild.id, 'serverLog', msg.channelMentions[0]);
  msg.channel.createMessage(`${config.em.o} Set server log channel to <#${msg.channelMentions[0]}>`);
}, {
  'description': 'Setup logs on your server',
  'usage': 'setup server [off / #channel]',
  'requirements': {
    'permissions': {
      'manageGuild': true
    }
  }
});