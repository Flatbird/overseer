module.exports = (client) => {
  client.reloadCommand = async (cat, cmd) => {
    const props = require(`./Commands/${cat}/${cmd}.js`);
    client.unregisterCommand(props.name, props.action, props.options);
    client.registerCommand(props.name, props.action, props.options);
  };
  client.resolveUser = function(usr) {
    if (!usr) return;
    usr = usr.replace(/[<,>,@,!]/ig, '');
    const results = [];
    const users = client.users.map(u => u.id);
    if (!parseInt(usr)) {
      users.forEach(user => {
        user = client.users.get(user);
        if (user.username.toLowerCase().search(usr.toLowerCase()) != -1) return results.push(user);
        else return;
      });
      return results;
    } else {
      if (!users.includes(usr)) return false;
      else return client.users.get(usr);
    }
  };
      
  client.resolveMember = function(gld, usr) {
    if (!gld) return console.log('Warning: client.resolveMember needs a guild id');
    if (!usr) return console.log('Warning: client.resolveMember needs a search query');
    if (!parseInt(gld)) return console.log('Warning: client.resolveMember -> Guild was not an id');
    const server = (client.guilds.has(gld)) ? client.guilds.get(gld) : false;
    if (server == false) return console.log('Warning: Server not found');
    usr = usr.replace(/[<,>,@,!]/ig, '');
    const results = [];
    const users = server.members.map(u => u.id);
    if (!parseInt(usr)) {
      users.forEach(user => {
        user = server.members.get(user);
        if (usr.length == 0) return console.error('Warning: client.resolveMember had an invalid user present. (When filtered)');
        if (user.user.username.toLowerCase().search(usr.toLowerCase()) != -1) return results.push(user);
        const uu = user.user.username + '#' + user.user.discriminator;
        if (uu.toLowerCase().search(usr.toLowerCase()) != -1) return results.push(user);
        else return;
      });
      return results;
    } else {
      if (!users.includes(usr)) return [];
      else return [server.members.get(usr)];
    }
       
  };

  client.getGuildSettings = (guild) => {
    const def = client.config.defaultSettings;
    if (!guild) return def;
    const returns = {};
    const overrides = client.settings.get(guild.id) || {};
    for (const key in def) {
      returns[key] = overrides[key] || def[key];
    }
    return returns;
  };
};