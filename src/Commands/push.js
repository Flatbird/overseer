/* eslint-disable */
const config = require("../config.js");
const { exec } = require("child_process");
module.exports = {
    name: "push",
    action: async (msg, args) => {
        try {
            const outputErr = (msg, stdData) => {
                const { stdout, stderr } = stdData;
                const message = stdout.concat(`\`\`\`${stderr}\`\`\``);
                msg.edit(message);
            };
              
            const doExec = (cmd, opts = {}) => {
                return new Promise((resolve, reject) => {
                    exec(cmd, opts, (err, stdout, stderr) => {
                        if (err) return reject({ stdout, stderr });
                        resolve(stdout);
                    });
                });
            };
            const command = `git add . && git commit -m "${args.join(" ")}" && git push`;
            //args.join(' ');
            const outMessage = await msg.channel.createMessage(`${config.em.w} Pushing code..`);
            //await msg.channel.send(`Running \`${command}\`...`);
            let stdOut = await doExec(command).catch(data=> outputErr(outMessage, data));
            stdOut = stdOut.substring(0, 1750);
            await outMessage.edit(`${config.em.o} Pushed to GitLab:
\`\`\`sh
${stdOut}
\`\`\``);
        } catch (err) {
            return;
        }
    },
    options: {
        "description": "updoots.",
        "usage": "push",
        "requirements": {
            "userIDs": config.owner
        }
    }
};