const config = require('../config.js');
const client = require('../client.js');
const axios = require('axios');
module.exports = {
  name: 'setav',
  action: async (msg, args) => {
    const url = args[0];
    try {
      const response = await axios.get(url, {
        headers: {
          'Accept': 'image/*'
        },
        responseType: 'arraybuffer'
      });
      client.editSelf({
        avatar: `data:${response.headers['content-type']};base64,${response.data.toString('base64')}`
      });
      await client.createMessage(msg.channel.id, `${config.em.o} Set avatar.`);

    } catch (e) {
      client.createMessage(msg.channel.id, `${config.em.e} ${e.message}`);
    }
  },
  options: {
    'description': 'Sets the bot\'s avatar.',
    'usage': 'setav [url]',
    'requirements': {
      'userIDs': config.owner
    }
  }
};