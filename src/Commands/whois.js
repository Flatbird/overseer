/* eslint-disable */
const config = require('../config.js');
const client = require('../client.js');
const moment = require('moment');
const status = {
    online: '<:status_online:508739077680660480> Online',
    idle: '<:status_idle:508739077386797066> Idle',
    dnd: '<:status_dnd:508739076292214785> Do Not Disturb',
    offline: '<:status_offline:508739077361762314> Offline/Invisible',
    streaming: '<:status_streaming:508739467981619211> Streaming'
  };
module.exports = {
    name: 'whois',
    action: async (msg, args) => {
        try {
        let member = '';
        if(!args[0]) member = await client.resolveMember(msg.channel.guild.id, msg.author.id)[0];
        if(args[0]) member = await client.resolveMember(msg.channel.guild.id, args[0])[0]
        if (member === false) return msg.channel.createMessage(`${config.em.e} Something broke. Contact <@!282586181856657409>.`);
        if (member == undefined) return msg.channel.createMessage(`${config.em.e} I can't find that user.`);
        
        let bot = 'error';
        if (member.user.bot === true) bot = 'Yes';
        else bot = 'No';
        function sortRoles(roles) {
            return roles.sort((a, b) => {
                return b.position - a.position;
            });
        }
        let roles = sortRoles(member.roles.map(r => msg.channel.guild.roles.get(r)))
        const highRole = member.roles.sort((a, b) => a.position < b.position)[0]
        const rlz = roles.map(i => i.mention).join(', ');
        
        
        const data = {
            embed: {
                //color: `${member.roles.length !== 0 ? member.roles.map(i => msg.channel.guild.roles.get(i)).filter(i => i.color).sort(function (a,b) { return b.position - a.position})[0].color : 0xF5DA33}`,
                author: {
                    name: member.user.username,
                    icon_url: member.user.avatarURL
                },
                thumbnail: {
                    url: member.user.avatarURL
                },
                timestamp: new Date(),
                fields: [{
                        name: 'ID',
                        value: member.id,
                        inline: true
                    },
                    {
                        name: 'Bot',
                        value: bot,
                        inline: true
                    },
                    {
                        name: 'Created At',
                        value: moment(member.user.createdAt).format('ll'),
                        inline: true
                    },
                    {
                        name: 'Joined At',
                        value: moment(member.joinedAt).format('ll'),
                        inline: true
                    },
                    {
                        name: 'Status',
                        value: `${status[member.status]}`,
                        inline: true
                    },
                    {
                        name: 'Game',
                        value: `${member.game ? `${member.game.name}` : 'Nothing'}`,
                        inline: true
                    },
                    {
                        name: 'Roles',
                        value: `${member.roles.length !== 0 ?  rlz : 'No Roles'}`,
                        inline: false
                    }
                ]
            }
        };
        msg.channel.createMessage(data)
    } catch (e) {
        msg.channel.createMessage(`${config.em.e} Something broke.`)
    }
    },
    options: {
        'description': 'Get information on a mentioned user.',
        'fullDescription': 'Get information on a mentioned user.',
        'requirements': {
            'userIDs': config.owner
        }
    }
};