const config = require('../config.js');
const client = require('../client.js');
module.exports = {
  name: 'shards',
  action: async (msg) => {
    try {
      const sd = [];
      client.shards.forEach((s) => {
        sd.push(`**Shard ${s.id}:**\nStatus: ${s.status}\nLatency: ${s.latency}ms\nGuilds: ${Object.keys(client.guildShardMap).filter((id) => client.guildShardMap[id] === s.id).length.toLocaleString()}`);
      });
      const data = {
        embed: {
          title: 'Shard Information',
          author: {
            name: client.user.username,
            icon_url: client.user.avatarURL
          },
          description: `${sd.join('\n')}`
        }
      };
      return msg.channel.createMessage(data);
    } catch (err) {
      return await msg.channel.createMessage(`${config.em.e} Error.`);
    }
  },
  options: {
    'description': 'Get info on all shards',
    'usage': 'shards',
  }
};