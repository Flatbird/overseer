/* eslint-disable */
const config = require("../config.js");
const { exec } = require("child_process");
module.exports = {
    name: "update",
    action: async (msg, args) => {
        try {
            const outputErr = (msg, stdData) => {
                const { stdout, stderr } = stdData;
                const message = stdout.concat(`\`\`\`${stderr}\`\`\``);
                msg.edit(message);
            };
              
            const doExec = (cmd, opts = {}) => {
                return new Promise((resolve, reject) => {
                    exec(cmd, opts, (err, stdout, stderr) => {
                        if (err) return reject({ stdout, stderr });
                        resolve(stdout);
                    });
                });
            };
            const command = "git pull";
            //args.join(' ');
            const outMessage = await msg.channel.createMessage(`${config.em.w} Updating..`);
            //await msg.channel.send(`Running \`${command}\`...`);
            let stdOut = await doExec(command).catch(data=> outputErr(outMessage, data));
            stdOut = stdOut.substring(0, 1750);
            await outMessage.edit(`${config.em.o} Pulled from GitLab\nRestarting...
\`\`\`sh
${stdOut}
\`\`\``);
            await process.exit(1);
        } catch (err) {
            return await msg.channel.createMessage(`${config.em.e} Error while updating\n\`\`\`xl\n${err.stack}\`\`\``);
        }
    },
    options: {
        "description": "updoots.",
        "usage": "update stuffs",
        "requirements": {
            "userIDs": config.owner
        }
    }
};