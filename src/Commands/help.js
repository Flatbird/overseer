const config = require('../config.js');
const client = require('../client.js');
module.exports = {
  name: 'help',
  action: async (msg, args) => {
    try {
      var result = '';
      if (args.length > 0) {
        var cur = client.commands[client.commandAliases[args[0]] || args[0]];
        if (!cur) {
          return `${config.em.e} I can't find that command.`;
        }
        var label = cur.label;
        for (var i = 1; i < args.length; ++i) {
          cur = cur.subcommands[cur.subcommandAliases[args[i]] || args[i]];
          if (!cur) {
            return `${config.em.e} I can't find that command.`;
          }
          label += ' ' + cur.label;
        }
        result += `**Command:** ${msg.prefix}${label}\n**Usage:** ${msg.prefix}${cur.usage}\n**Description:** ${cur.description}`;
        if (Object.keys(cur.aliases).length > 0) {
          result += `\n**Aliases:** ${cur.aliases.join(', ')}`;
        }
        if (Object.keys(cur.subcommands).length > 0) {
          result += '\n**Subcommands:**';
          for (var subLabel in cur.subcommands) {
            if (cur.subcommands[subLabel].permissionCheck(msg)) {
              result += `\n   **${msg.prefix}${label} ${subLabel}** - ${cur.subcommands[subLabel].description}`;
            }
          }
        }
        await client.createMessage(msg.channel.id, {embed: {
          description: result,
          thumbnail: {
            url: client.user.avatarURL
          },
          color: 0x38393E
        }});
      } else {
        result += `${client.commandOptions.name} - Commands List:\n`;
        result += '**Need help? Join our support server! [https://discord.gg/3YXKs3s](https://discord.gg/3YXKs3s)**\n\n';
        result += '**Commands:**\n';
        for (label in client.commands) {
          if (client.commands[label] && client.commands[label].permissionCheck(msg) && !client.commands[label].hidden) {
            result += `  **${msg.prefix}${label}** - ${client.commands[label].description}\n`;
          }
        }
        result += `\nType ${msg.prefix}help <command> for more info on a command.`;
                
        const dm = await client.getDMChannel(msg.author.id);
        await dm.createMessage({ embed: {
          description: result,
          thumbnail: {
            url: client.user.avatarURL
          },
          color: 0x38393E
        }});
        await msg.channel.createMessage(`${config.em.o} Check your DMs!`);
      }
    } catch (err) {
      if (err.message.includes(50007)) return msg.channel.createMessage(`${config.em.e} I can't DM you. Try enabling your DMs.`);
      msg.channel.createMessage(`${config.em.e} I was unable to DM you.`);
    }
  },
  options: {
    'description': 'Get this list of commands.',
    'usage': 'help [optional command name]',
    'fullDescription': 'Get a list of commands.'
  }
};    