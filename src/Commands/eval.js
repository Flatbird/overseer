const client = require('../client.js');
const config = require('../config.js');
const { inspect } = require('util');
module.exports = {
  name: 'eval',
  action: async (msg, args) => {
    let evaled = '';
    try {
      evaled = await eval(args.join(' ').trim());
    
      switch (typeof evaled) {
        case 'object':
          evaled = inspect(evaled, {
            depth: 0
          });
          break;
        default:
      }
            
    } catch (err) {
      return msg.channel.createMessage(`\`\`\`js\n${err.message}\`\`\``);
    }
    
    if (typeof evaled === 'string') {
      evaled = evaled.replace(client.token, 'https://giphy.com/gifs/greatest-advert-u5u5HQbmzwhzi ');
    }
    if (evaled == undefined) {
      evaled = 'undefined';
    }
    if (evaled.length > 1900) {
      evaled = 'Response too large';
    }
    
    return msg.channel.createMessage(`\`\`\`js\n${evaled}\`\`\``);
  },
  options: {
    'description': 'Evaluate JS code.',
    'fullDescription': 'Evaluate JS code from Discord.',
    'usage': 'eval [code]',
    'aliases': ['e', 'ev', 'suckajsdick', 'pornhub'],
    'requirements': {
      userIDs: config.owner
    }
  }
};    