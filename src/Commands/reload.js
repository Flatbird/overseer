const config = require('../config.js');
const client = require('../client.js');
const Aclient = require('../dev.js');
const reload = require('require-reload')(require);
module.exports = {
  name: 'reload',
  action: async (msg, args) => {
    try {
      client.unregisterCommand(args[0]);
      Aclient.unregisterCommand(args[0]);
      const c = reload(`./${args[0]}`);
      client.registerCommand(c.name, c.action, c.options);
      Aclient.registerCommand(c.name, c.action, c.options);
        
      return await msg.channel.createMessage(`${config.em.o} Reloaded \`${args[0]}\``);
    } catch (err) {
      return await msg.channel.createMessage(`${config.em.e} Error while loading command \`${args[0]}\`!\n\`\`\`xl\n${err.stack}\`\`\``);
    }
  },
  options: {
    'description': 'Reloads the specified command.',
    'usage': 'reload [command]',
    'requirements': {
      'userIDs': config.admins
    }
  }
};