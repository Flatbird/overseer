const client = require('../client.js');
module.exports = {
  name: 'uptime',
  action: async (msg) => {
    msg.channel.createMessage(`**${~~(client.uptime / 86400000)}** days, **${~~((client.uptime / 3600000) % 24)}** hours, **${~~((client.uptime / 60000) % 60)}** min, **${~~((client.uptime / 1000) % 60)}** sec`);
  },
  options: {
    'description': 'Get the bot\'s uptime.',
    'usage': 'uptime',
    'fullDescription': 'Get the bot\'s uptine.',
    'aliases': ['up']
  }
};    