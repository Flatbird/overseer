//const client = require('../client.js');
module.exports = {
  name: 'info',
  action: async (/*msg*/) => {
    return '<:red_tick:508736383947505674> This command is disabled';/*
        const data = {
            embed: {
                color: 0xF5D833,
                author: {
                    name: client.user.username,
                    icon_url: client.user.avatarURL
                },
                fields: [
                    {
                        name: 'Library',
                        value: 'Eris',
                        inline: true
                    },
                    {
                        name: 'Memory Usage',
                        value: `${(process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2)} MB`,
                        inline: true
                    },
                    {
                        name: 'Version',
                        value: '3.0.0',
                        inline: true
                    },
                    {
                        name: 'Users',
                        value: client.users.size,
                        inline: true
                    },
                    {
                        name: 'Servers',
                        value: client.guilds.size,
                        inline: true
                    },
                    {
                        name: 'Developer',
                        value: 'Flatbird#0001',
                        inline: true
                    },
                    {
                        name: 'Support',
                        value: '[Click Here](http://b1rdbot.tk/discord)',
                        inline: true
                    },
                    {
                        name: 'Bot Invite',
                        value: '[Click Here](http://b1rdbot.tk/invite)',
                        inline: true
                    },
                ],
                thumbnail: {
                    url: client.user.avatarURL
                },
                timestamp: new Date(),
                footer: {
                    text: 'You are on shard ' + msg.channel.guild.shard.id + '/' + client.shards.size
                }
            }
        };

        client.createMessage(msg.channel.id, data);*/
  },
  options: {
    'description': 'Get information on the bot.',
    'usage': 'info',
    'fullDescription': 'Get information on the bot.'
  }
};    