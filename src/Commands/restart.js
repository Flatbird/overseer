const client = require('../client.js');
const config = require('../config.js');
module.exports = {
  name: 'restart',
  action: async (msg, args) => {
    if (args[0]) {
      if (args[0] === '-d') {
        await msg.channel.createMessage(`${config.em.o} Reconnecting \`${config.maxShards}\` shards`);
        client.disconnect();
        await client.connect();
        return;
      }
      const s = parseInt(args[0]);
      client.shards.get(s).disconnect();
      client.shards.get(s).connect();
      msg.channel.createMessage(`${config.em.o} Restarting shard \`${s}\``);
    } else {
      await msg.channel.createMessage(`${config.em.o} Exiting with code \`0\``);
      process.exit(0);
    }
  },
  options: {
    'description': 'Restart a shard or the whole process.',
    'fullDescription': 'Restart a shard or the whole process.',
    'usage': 'restart [optional shard id]',
    'aliases': ['r'],
    'requirements': {
      'userIDs': config.owner,
    }
  }
};    