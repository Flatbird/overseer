module.exports = async (client, guild) => {
  client.logger.log(guild.name + ' has added the bot. Shard ' + guild.shard.id);
  const dm = await client.getDMChannel('282586181856657409');
  dm.createMessage('[GUILD]: ' + guild.name + ' has added the bot. Shard ' + guild.shard.id);
};