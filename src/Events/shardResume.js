const config = require('../config.js');
module.exports = (client, id) => {
  const data = {
    embeds: [
      {
        title: `Shard ${id} Resumed`,
        color: 0xECB250,
        timestamp: new Date(),
        footer: {
          text: 'Overseer ' + config.env
        }
      }
    ]
  };
  client.executeWebhook(config.wh.s.i, config.wh.s.t, data);
  client.logger.ded(`Shard ${id} has resumed`);
};