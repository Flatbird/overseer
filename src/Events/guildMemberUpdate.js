module.exports = (client, guild, newMember, oldMember) => {
  if (!client.settings.has(guild.id)) return;
  const set = client.settings.get(guild.id);
  const chan = guild.channels.get(set.userLog);
  if (chan == undefined || chan == 'disabled' || chan == null) return;
  if (!chan) return;
  const member = newMember;
  if (oldMember.nick !== newMember.nick) {
    const oldNick = (oldMember.nick) ? oldMember.nick : newMember.username;
    const newNick = (newMember.nick) ? newMember.nick : newMember.username;
        
    const data = {
      embed: {
        author: {
          name: member.user.username + '#' + member.user.discriminator,
          icon_url: member.user.avatarURL
        },
        title: `${member.user.username} changed their nickname`,
        fields: [ { name: 'Before', value: oldNick, inline: true}, { name: 'After', value: newNick, inline: true } ],
        footer: { text: `Member ID: ${member.id}` },
        color: 0xffff00,
        timestamp: new Date()
      }
    };
    client.createMessage(chan.id, data);
  } 
  else if (newMember.roles.length != oldMember.roles.length) {
    const oldRoles = [];
    const newRoles = [];
    for (let i=0; i<oldMember.roles.length; i++) {
      oldRoles.push(guild.roles.get(oldMember.roles[i]).name);
    }
    for (let i=0; i<newMember.roles.length; i++) {
      newRoles.push(guild.roles.get(newMember.roles[i]).name);
    }
    const data = {
      embed: {
        author: {
          name: member.user.username + '#' + member.user.discriminator,
          icon_url: member.user.avatarURL
        },
        title: `${member.user.username}'s roles have updated`,
        fields: [ { name: 'Before', value: `${oldRoles.length ? oldRoles.join(', ') : 'None'}`, inline: false}, { name: 'After', value: `${newRoles.length ? newRoles.join(', ') : 'None'}`, inline: true } ],
        footer: { text: `Member ID: ${member.id}` },
        color: 0xffff00,
        timestamp: new Date()
      }
    };
    client.createMessage(chan.id, data);
  }
};