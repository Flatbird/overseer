const { inspect } = require('util');
module.exports = (client) => {
  client.logger.ready('Ready!');
  client.shards.forEach((s) => {
    s.editStatus('online', { name: `${client.config.prefix[0]}setup | Shard ${s.id}`});
  });
  client.logger.shard((inspect(client.guildShardMap, {
    depth: 0
  })));
};