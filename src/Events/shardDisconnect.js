const config = require('../config.js');
module.exports = (client, err, id) => {
  if (err) {
    const data = {
      embeds: [
        {
          title: `Shard ${id} Disconnected`,
          color: 0xBE3245,
          timestamp: new Date(),
          fields: [
            {
              name: 'Error',
              value: err ? err : 'Server didn\'t acknowledge previous heartbeat, possible lost connection'
            }
          ],
          footer: {
            text: 'Overseer ' + config.env
          }
        }
      ]
    };
    client.executeWebhook(config.wh.s.i, config.wh.s.t, data);
    return client.logger.ded(`Shard ${id} has disconnected, ${err}`);
  } else {
    const data = {
      embeds: [
        {
          title: `Shard ${id} Disconnected`,
          color: 0xBE3245,
          timestamp: new Date(),
          footer: {
            text: 'Overseer ' + config.env
          }
        }
      ]
    };
    client.executeWebhook(config.wh.s.i, config.wh.s.t, data);
    client.logger.ded(`Shard ${id} has disconnected`);
  }
};