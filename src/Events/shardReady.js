const config = require('../config.js');
module.exports = (client, id) => {
  const data = {
    embeds: [
      {
        title: `Shard ${id} Connected`,
        color: 0x9CDC7D,
        timestamp: new Date(),
        footer: {
          text: 'Overseer ' + config.env
        }
      }
    ]
  };
  client.executeWebhook(config.wh.s.i, config.wh.s.t, data);
  client.logger.shard(`Shard ${id} is ready`);
};