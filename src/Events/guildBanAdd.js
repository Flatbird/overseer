module.exports = (client, guild, user) => {
  if (!client.settings.has(guild.id)) return;
  const set = client.settings.get(guild.id);
  const chan = guild.channels.get(set.serverLog);
  if (chan == undefined || chan == 'disabled' || chan == null) return;
  if (!chan) return;

  const data = {
    embed: {
      author: {
        name: `${guild.name}`,
        icon_url: guild.iconURL
      },
      title: `${user.username}#${user.discriminator} was banned`,
      footer: { text: `Member ID: ${user.id}` },
      color: 0xFF0000,
      timestamp: new Date()
    }
  };
  client.createMessage(chan.id, data);
};