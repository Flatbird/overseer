module.exports = (client, guild, member) => {
  if (!client.settings.has(guild.id)) return;
  const set = client.settings.get(guild.id);
  const chan = guild.channels.get(set.serverLog);
  if (chan == undefined || chan == 'disabled' || chan == null) return;
  if (!chan) return;
    
  const data = {
    embed: {
      author: {
        name: `${guild.name}`,
        icon_url: guild.iconURL
      },
      title: `${member.user.username} has left`,
      fields: [ { name: 'Member Count', value: member.guild.memberCount, inline: true } ],
      footer: { text: `Member ID: ${member.id}` },
      color: 0xFF7F00,
      timestamp: new Date()
    }
  };
  client.createMessage(chan.id, data);
};