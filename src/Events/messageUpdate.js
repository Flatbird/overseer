module.exports = (client, msg, oldMessage) => {
  if (!msg.channel.type == 0) return;
  if (!msg.author) return;
  if (!msg.author.bot == false) return;
  if (!msg.guild) return;
  if (!client.settings.has(msg.guild.id)) return;
  const set = client.settings.get(msg.guild.id);
  const chan = msg.guild.channels.get(set.messageLog);
  if (chan == undefined || chan == 'disabled' || chan == null) return;
  if (!chan) return;
  const mauth = msg.author;
  const mauthtag = msg.author.username + '#' + msg.author.discriminator;
  if (msg.content == oldMessage.content) return;
    
  const data = {
    embed: {
      author: {
        name: mauthtag,
        icon_url: mauth.avatarURL
      },
      title: `Message edited in #${msg.channel.name}`,
      fields: [ { name: 'Before', value: `${oldMessage.content ? oldMessage.content : '*Error getting message.*'}` }, { name: 'After', value: `${msg.content ? msg.content : '*Error getting message.*'}` } ],
      footer: { text: `Author ID: ${mauth.id}` },
      color: 0xffb861,
      timestamp: new Date()
    }
  };
  client.createMessage(chan.id, data);
};