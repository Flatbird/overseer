const moment = require('moment');
module.exports = (client, guild, member) => {
  if (!client.settings.has(guild.id)) return;
  const set = client.settings.get(guild.id);
  const chan = guild.channels.get(set.serverLog);
  if (chan == undefined || chan == 'disabled' || chan == null) return;
  if (!chan) return;
    
  if ((new Date(member.user.createdAt)).getTime() > (Date.now()-432000000)) {
    const data = {
      embed: {
        author: {
          name: `${member.user.username}#${member.user.discriminator}`,
          icon_url: member.user.avatarURL
        },
        title: `${member.user.username} has joined`,
        description: '⚠️ New account',
        fields: [ { name: 'Account Created', value: moment(member.user.createdAt).format('ll'), inline: true}, { name: 'Member Count', value: member.guild.memberCount, inline: true } ],
        footer: { text: `Member ID: ${member.id}` },
        color: 0x77dd77,
        timestamp: new Date()
      }
    };
    return client.createMessage(chan.id, data);
  }

  const data = {
    embed: {
      author: {
        name: `${member.user.username}#${member.user.discriminator}`,
        icon_url: member.user.avatarURL
      },
      title: `${member.user.username} has joined`,
      fields: [ { name: 'Account Created', value: moment(member.user.createdAt).format('ll'), inline: true}, { name: 'Member Count', value: member.guild.memberCount, inline: true } ],
      footer: { text: `Member ID: ${member.id}` },
      color: 0x77dd77,
      timestamp: new Date()
    }
  };
  client.createMessage(chan.id, data);
};